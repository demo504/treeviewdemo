# TreeViewDemo

## Controle TreeView C#

É um controle existente na plataforma .NET

É utilizado para exibir dados de natureza hierarquíca, ele possui uma estrutura composta por árvore de nós(node). Dai o nome tree(árvore) view(visão).

Uma 'árvore'(tree) é constituída por ramificações, que por sua vez são constituídas por muitos 'nós' (node), onde cada nó consiste de uma imagem (configurada através da propriedade Image) e um rótulo ( configurado via propriedade Text ). As imagens usadas nos nós(node) são fornecidas pela associação de um componente ImageList com o Controle TreeView. A propriedade ImageList do controle TreeView define o ImageList que contém os objetos Image usados nos nós da árvore.

Um nó(node) pode ser expandido ou retraído, dependendo se possuir ou não nós filhos(child nodes) descendentes. No nível superior ficam as raízes (root nodes) , e cada nó raiz pode ter qualquer quantidade de nós filhos.

As propriedades principais do controle TreeView são Nodes e SelectedNode.

A propriedade Nodes contém a lista de nós de nível superior na exibição em árvore;
A propriedade SelectedNode define o nó atualmente selecionado;

