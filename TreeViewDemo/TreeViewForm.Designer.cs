﻿
namespace TreeViewDemo
{
    partial class TreeViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DocumentosTreeView = new System.Windows.Forms.TreeView();
            this.btnGetItensSelecionados = new System.Windows.Forms.Button();
            this.btnPreencheDados = new System.Windows.Forms.Button();
            this.btnRemoveItemSelecionado = new System.Windows.Forms.Button();
            this.btnRemoveNoCompleto = new System.Windows.Forms.Button();
            this.btnLimpaTreeView = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DocumentosTreeView
            // 
            this.DocumentosTreeView.CheckBoxes = true;
            this.DocumentosTreeView.Location = new System.Drawing.Point(6, 19);
            this.DocumentosTreeView.Name = "DocumentosTreeView";
            this.DocumentosTreeView.Size = new System.Drawing.Size(218, 253);
            this.DocumentosTreeView.TabIndex = 0;
            this.DocumentosTreeView.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.DocumentosTreeView_BeforeCheck);
            // 
            // btnGetItensSelecionados
            // 
            this.btnGetItensSelecionados.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetItensSelecionados.Location = new System.Drawing.Point(239, 104);
            this.btnGetItensSelecionados.Name = "btnGetItensSelecionados";
            this.btnGetItensSelecionados.Size = new System.Drawing.Size(171, 23);
            this.btnGetItensSelecionados.TabIndex = 1;
            this.btnGetItensSelecionados.Text = "Get itens selecionados";
            this.btnGetItensSelecionados.UseVisualStyleBackColor = true;
            this.btnGetItensSelecionados.Click += new System.EventHandler(this.btnGetItensSelecionados_Click);
            // 
            // btnPreencheDados
            // 
            this.btnPreencheDados.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPreencheDados.Location = new System.Drawing.Point(239, 75);
            this.btnPreencheDados.Name = "btnPreencheDados";
            this.btnPreencheDados.Size = new System.Drawing.Size(171, 23);
            this.btnPreencheDados.TabIndex = 3;
            this.btnPreencheDados.Text = "Preeche Dados Default";
            this.btnPreencheDados.UseVisualStyleBackColor = true;
            this.btnPreencheDados.Click += new System.EventHandler(this.btnPreencheDados_Click);
            // 
            // btnRemoveItemSelecionado
            // 
            this.btnRemoveItemSelecionado.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveItemSelecionado.Location = new System.Drawing.Point(239, 133);
            this.btnRemoveItemSelecionado.Name = "btnRemoveItemSelecionado";
            this.btnRemoveItemSelecionado.Size = new System.Drawing.Size(171, 23);
            this.btnRemoveItemSelecionado.TabIndex = 4;
            this.btnRemoveItemSelecionado.Text = "Remove itens selecionados";
            this.btnRemoveItemSelecionado.UseVisualStyleBackColor = true;
            // 
            // btnRemoveNoCompleto
            // 
            this.btnRemoveNoCompleto.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveNoCompleto.Location = new System.Drawing.Point(239, 162);
            this.btnRemoveNoCompleto.Name = "btnRemoveNoCompleto";
            this.btnRemoveNoCompleto.Size = new System.Drawing.Size(171, 23);
            this.btnRemoveNoCompleto.TabIndex = 5;
            this.btnRemoveNoCompleto.Text = "Remove Nó completo";
            this.btnRemoveNoCompleto.UseVisualStyleBackColor = true;
            // 
            // btnLimpaTreeView
            // 
            this.btnLimpaTreeView.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpaTreeView.Location = new System.Drawing.Point(239, 191);
            this.btnLimpaTreeView.Name = "btnLimpaTreeView";
            this.btnLimpaTreeView.Size = new System.Drawing.Size(171, 23);
            this.btnLimpaTreeView.TabIndex = 6;
            this.btnLimpaTreeView.Text = "Limpa árvore";
            this.btnLimpaTreeView.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DocumentosTreeView);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 276);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Documentos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(106, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Operações Com TrueView";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 344);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnLimpaTreeView);
            this.Controls.Add(this.btnRemoveNoCompleto);
            this.Controls.Add(this.btnRemoveItemSelecionado);
            this.Controls.Add(this.btnPreencheDados);
            this.Controls.Add(this.btnGetItensSelecionados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TreeView Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView DocumentosTreeView;
        private System.Windows.Forms.Button btnGetItensSelecionados;
        private System.Windows.Forms.Button btnPreencheDados;
        private System.Windows.Forms.Button btnRemoveItemSelecionado;
        private System.Windows.Forms.Button btnRemoveNoCompleto;
        private System.Windows.Forms.Button btnLimpaTreeView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
    }
}

