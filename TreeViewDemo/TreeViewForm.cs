﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TreeViewDemo
{
    public partial class TreeViewForm : Form
    {
        public TreeViewForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            preencheDados();
        }


        private List<Documento> getDocsVenda()
        {
            Documento doc1 = new Documento() { TipoDoc = "FA", Descricao = "Factura" };
            Documento doc2 = new Documento() { TipoDoc = "ECL", Descricao = "Encomenda" };
            Documento doc3 = new Documento() { TipoDoc = "OR", Descricao = "Orçamento" };
            Documento doc4 = new Documento() { TipoDoc = "VD", Descricao = "Venda a dinheiro" };

            List<Documento> docs = new List<Documento>();

            docs.Add(doc1);
            docs.Add(doc2);
            docs.Add(doc3);
            docs.Add(doc4);

            return docs;
        }

        private List<Documento> getDocsTES()
        {
            Documento doc1 = new Documento() { TipoDoc = "ABTCX", Descricao = "Abertura da Caixa" };
            Documento doc2 = new Documento() { TipoDoc = "MOV", Descricao = "Movimento de Conta" };
            Documento doc3 = new Documento() { TipoDoc = "FCHCX", Descricao = "Fecho da Caixa" };


            List<Documento> docs = new List<Documento>();

            docs.Add(doc1);
            docs.Add(doc2);
            docs.Add(doc3);

            return docs;
        }

        private void preencheDados()
        {
            Dictionary<string, List<Documento>> arvore = new Dictionary<string, List<Documento>>();
            arvore.Add("Vendas", getDocsVenda());
            arvore.Add("Tesouraria", getDocsTES());

            DocumentosTreeView.Nodes.Add("Vendas");
            DocumentosTreeView.Nodes.Add("Tesouraria");

            foreach (KeyValuePair<string, List<Documento>> item in arvore)
            {
                var no_principal = item.Key.ToString();

                switch (no_principal)
                {
                    case "Vendas":
                        foreach (Documento _item in item.Value)
                        {
                            DocumentosTreeView.Nodes[0].Nodes.Add(_item.TipoDoc + " - " + _item.Descricao);
                        }
                        break;
                    case "Tesouraria":
                        foreach (Documento _item in item.Value)
                        {
                            DocumentosTreeView.Nodes[1].Nodes.Add(_item.TipoDoc + " - " + _item.Descricao);
                        }
                        break;
                    
                }
                
            }



        }

        private void btnPreencheDados_Click(object sender, EventArgs e)
        {
            preencheDados();
        }

       
        private Dictionary<string, List<Documento>> GetDocsSelecionados()
        {
            List<Documento> nodeValues = null;
            Documento       doc = null;
            Dictionary<string, List<Documento>> docsSelecionados = new Dictionary<string, List<Documento>>();

            foreach (TreeNode node in DocumentosTreeView.Nodes)
            {
                nodeValues = new List<Documento>();

                switch (node.Text.ToString())
                {
                    case "Vendas":
                        // TODO
                        //if (node.Checked)
                        //{
                            foreach (TreeNode node_filho in DocumentosTreeView.Nodes[0].Nodes)
                            {
                                if (node_filho.Checked)
                                {
                                    doc = new Documento();
                                    string[] docSel = node_filho.Text.Split('-');
                                    doc.TipoDoc = docSel[0].ToString();
                                    doc.Descricao = docSel[1].ToString();
                                    nodeValues.Add(doc);
                                }
                            }
                            docsSelecionados.Add(node.Text.ToString(), nodeValues);
                        //}
                        
                        break;

                    case "Tesouraria":

                        //if (node.Checked)
                        //{
                            foreach (TreeNode node_filho in DocumentosTreeView.Nodes[1].Nodes)
                            {
                                if (node_filho.Checked)
                                {
                                    doc = new Documento();
                                    string[] docSel = node_filho.Text.Split('-');
                                    doc.TipoDoc = docSel[0].ToString();
                                    doc.Descricao = docSel[1].ToString();
                                    nodeValues.Add(doc);
                                }

                            }
                            docsSelecionados.Add(node.Text.ToString(), nodeValues);
                        //}
                        break;
                }
            }
            return docsSelecionados;
        }

        private void btnGetItensSelecionados_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<Documento>> docs = GetDocsSelecionados();

            foreach (KeyValuePair<string, List<Documento>> chave in docs)
            {
                string docsByKey = $"{chave.Key}: {Environment.NewLine}";
                foreach (Documento valor in chave.Value)
                {
                    docsByKey += $",{valor.TipoDoc}-{valor.Descricao}";
                }
                MessageBox.Show(docsByKey);

            }
        }

        /// <summary>
        /// Depois de fazer o clique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocumentosTreeView_BeforeCheck(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.Text.Equals("Vendas"))
            {
                foreach (TreeNode item in e.Node.Nodes)
                {
                    // TODO - TESTE: isto funciona correctamente ? 
                    if (e.Node.Checked.Equals(false))
                    {
                        item.Checked = true;
                    }
                    else
                    {
                        item.Checked = false;
                    }
                    
                }
                e.Node.Expand();
            }

            if (e.Node.Text.Equals("Tesouraria"))
            {
                foreach (TreeNode item in e.Node.Nodes)
                {
                    item.Checked = true;
                }
                e.Node.Expand();
            }

            // TODO - Caso for selecionado um filho, selecionar automaticamente o pai.
            /* Possível problema: Com a funcionalidade a cima implementada é possível que ao selecionar 
             * automaticamente o pai todos os filhos sejam selecionados */
        }
    }
    public class Documento
    {
        public string TipoDoc { get; set; }
        public string Descricao { get; set; }
    }
}
